
from django.contrib import admin
from django.urls import path
from django.urls.conf import include, include, re_path
from django.conf import settings
admin.site.site_header = settings.ADMIN_SITE_HEADER

urlpatterns = [
    path('', include(('core.apps.base.urls','base'), namespace='base')),
    path('', include(('core.apps.urls', 'academico'), namespace='academico')),  
    path('', include('authentication.urls')), # Auth routes - login / register
    path('jet/', include('jet.urls', 'jet')),
    path('admin/', admin.site.urls),
    path('reports/', include(('core.reports.urls','reports'), namespace='reports')),   
    path('accounts/', include('django.contrib.auth.urls')), 
    
]
