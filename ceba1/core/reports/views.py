from cgitb import html
from datetime import datetime
from multiprocessing import context
from re import template
import string
from unittest import result
from urllib import response
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView
from core.apps.models import Pagos
from core.reports.forms import *
from django.views.decorators.csrf import csrf_exempt

import io
from django.http import FileResponse
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Image, Table, Spacer

from django.template.loader import render_to_string
from django.template.loader import get_template
from weasyprint import HTML
import tempfile
from django.db.models import Sum

class ReportPagosView(TemplateView):
    template_name = 'pagos/report.html'

    

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'search_report':
                data = []
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)        
        context['form'] = ReportPagoForm()
        return context

#=============================================================================================#      
#===================================-->Reportes PDF - ReportLab<--============================# 
def pagos_pdf(request):
    buf=io.BytesIO()
    c=canvas.Canvas(buf, pagesize=A4, bottomup=0)
    textobj=c.beginText()
    textobj.setTextOrigin(inch, inch)
    textobj.setFont("Helvetica", 12)      
    
    #Add some lines of text
   
    pagos=Pagos.objects.all()

    lines=[]

    

    for pago in pagos:
        lines.append(str(pago.id))
        lines.append(str(pago.idestudiante))
        lines.append(str(pago.fecha_pago))
        lines.append(str(pago.monto))
        lines.append(str(pago.concepto))
        lines.append(str(pago.pago_condicion))
        lines.append("===============================")
    

    for line in lines:
        textobj.textLine(line) 
    c.drawImage('https://www.cebasanagustin.edu.pe/wp-content/uploads/2020/11/logo-ceba-mod.png', 1 * inch, 1 * inch)
    c.drawText(textobj)
    c.drawString(10,20, "Reporte de Pagos - CEBA SanAgustin")
    c.showPage()
    c.save()
    buf.seek(0)

    return FileResponse(buf, as_attachment=True, filename='pagos_pdf')


