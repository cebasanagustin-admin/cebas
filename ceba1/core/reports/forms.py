from django.forms import *

class ReportPagoForm(Form):
    date_range = CharField(widget=TextInput(attrs={
        'class':'form-control',
        'autocomplete':'off',
    }))