from django.contrib import admin

from .models import *

# Register your models here.
class CursoAdmin(admin.ModelAdmin):
	list_display = ['fullname', 'shortname', 'category']

class CalificacionAdmin(admin.ModelAdmin):
	list_display = ['idestudiante', 'idcurso', 'bimestre', 'asistencia', 'intervencion', 'tarea', 'practica', 'evaluacion', 'media_notas']



admin.site.register(Mdl2HAssignGrades)
admin.site.register(Mdl2HCourse, CursoAdmin)
admin.site.register(Mdl2HUser)
admin.site.register(Mdl2HCourseCategories)
admin.site.register(Docente)
admin.site.register(Pagos)
admin.site.register(Calificacion, CalificacionAdmin)