from django import forms
from .models import *

############################--Curso--############################

class CursoForm(forms.ModelForm):
    class Meta:
        model = Mdl2HCourse
    
        fields = ['fullname', 'shortname', 'category', 'summary', 'nivel', 'grado', 'turno']
        widgets = {
            'fullname': forms.TextInput(attrs={'class':'form-control'}),
            'shortname': forms.TextInput(attrs={'class':'form-control'}),
            'category': forms.TextInput(attrs={'class':'form-control'}),            
            'summary': forms.Textarea(attrs={'class':'form-control'}),
            'nivel': forms.Select(attrs={'class':'form-control'}),
            'grado': forms.Select(attrs={'class':'form-control'}),
            'turno': forms.Select(attrs={'class':'form-control'}),

        }

############################--Asistencia--############################

class AsistenciaForm(forms.ModelForm):
    class Meta:
        model = Mdl2HAttendanceLog
    
        fields = ['studentid', 'statusid']
        widgets = {
            'studentid': forms.TextInput(attrs={'placeholder': 'CURSO', 'class':'form-control'}),
            'statusid': forms.TextInput(attrs={'placeholder': 'CURSO', 'class':'form-control'}),            

        }

############################--Estudiante--############################

class EstudianteForm(forms.ModelForm):
    class Meta:
        model = Mdl2HUser
    
        fields = ['firstname', 'lastname', 'address', 'city', 'phone1', 'phone2', 'email', 'dni', 'dniapoderado', 'napoderado',
        'apeapoderado', 'telefonoapo', 'nmatricula', 'nivel', 'grado', 'turno', 'username', 'password']
        widgets = {
            'dni': forms.TextInput(attrs={'class':'form-control'}),
            'firstname': forms.TextInput(attrs={'class':'form-control'}),            
            'lastname': forms.TextInput(attrs={'class':'form-control'}), 
            'napoderado': forms.TextInput(attrs={'class':'form-control'}),      
            'address': forms.TextInput(attrs={'class':'form-control'}),      
            'city': forms.TextInput(attrs={'class':'form-control'}),      
            'phone1': forms.TextInput(attrs={'class':'form-control'}),     
            'phone2': forms.TextInput(attrs={'class':'form-control'}),     
            'email': forms.EmailInput(attrs={'class':'form-control'}),      
            'dniapoderado': forms.TextInput(attrs={'class':'form-control'}),      
            'apeapoderado': forms.TextInput(attrs={'class':'form-control'}), 
            'telefonoapo': forms.TextInput(attrs={'class':'form-control'}), 
            'nmatricula': forms.TextInput(attrs={'class':'form-control'}),      
            'nivel': forms.Select(attrs={'class':'form-control'}),      
            'grado': forms.Select(attrs={'class':'form-control'}),      
            'turno': forms.Select(attrs={'class':'form-control'}),   
            'username': forms.TextInput(attrs={'class':'form-control'}),     
            'password': forms.PasswordInput(attrs={'class':'form-control'}),        

        }

############################--Pagos--############################

class PagosForm(forms.ModelForm):
    class Meta:
        model = Pagos
    
        fields = ['id', 'idestudiante', 'fecha_pago', 'monto', 'concepto', 'voucher', 'pago_condicion']
        widgets = {
            'id': forms.TextInput(attrs={'placeholder': 'ID','class':'form-control'}),
            'idestudiante': forms.Select(attrs={'class':'form-control'}),            
            'fecha_pago' : forms.DateInput(attrs={'placeholder': '01/01/2022', 'class':'form-control', 'autocomplete':'off'}),
            'monto' : forms.NumberInput(attrs={'placeholder': 'MONTO', 'class':'form-control'}),
            'concepto' : forms.TextInput(attrs={'placeholder': 'CONCEPTO', 'class':'form-control', 'autocomplete':'off'}),
            'pago_condicion' : forms.CheckboxInput(attrs={'placeholder': 'PAGO', 'class':'form-control'}),
           
        }    


############################--Calificación--############################

class CalificacionForm(forms.ModelForm):
    class Meta:
        model = Calificacion

        #codestudiante: forms.ModelMultipleChoiceField(queryset=Estudiante.objects.all(), to_field_name="codestudiante_name", widget=forms.Select(attrs={'placeholder': 'CÓDIGO','class':'form-control'}))
    
        fields = ['bimestre', 'idestudiante', 'idcurso', 'asistencia', 'intervencion', 'tarea', 'practica', 'evaluacion']
        widgets = {
            'bimestre': forms.Select(attrs={'placeholder': 'CÓDIGO','class':'form-control'}),
            'idestudiante': forms.Select(attrs={'placeholder': 'ESTUDIANTE','class':'form-control'}),            
            'idcurso': forms.Select(attrs={'placeholder': 'CURSO','class':'form-control'}),
            'asistencia': forms.NumberInput(attrs={'placeholder': 'ASIS', 'class':'form-control'}),
            'intervencion': forms.NumberInput(attrs={'placeholder': 'INTE', 'class':'form-control'}),    
            'tarea': forms.NumberInput(attrs={'placeholder': 'TARE', 'class':'form-control'}),            
            'practica': forms.NumberInput(attrs={'placeholder': 'PRAC', 'class':'form-control'}),        
            'evaluacion': forms.NumberInput(attrs={'placeholder': 'EVAL', 'class':'form-control'}),

        }


############################--Docente--############################

class DocenteForm(forms.ModelForm):
    class Meta:
        model = Docente
    
        fields = ['dni', 'ndocente', 'apedocente', 'direccion', 'telefono', 'email', 'profesion', 'especialidad', 'experiencia']
        widgets = {
            'dni': forms.TextInput(attrs={'class':'form-control'}),
            'ndocente': forms.TextInput(attrs={'class':'form-control'}),
            'apedocente': forms.TextInput(attrs={'class':'form-control'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.EmailInput(attrs={'class':'form-control'}),
            'profesion': forms.TextInput(attrs={'class':'form-control'}),
            'especialidad': forms.TextInput(attrs={'class':'form-control'}),         
            'experiencia': forms.TextInput(attrs={'placeholder': 'En números', 'class':'form-control'}),             

        }