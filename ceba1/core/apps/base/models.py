from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Base(models.Model):
    id = models.AutoField(primary_key=True)
    estado = models.BooleanField('Estado', default=True)
    fec_c = models.DateTimeField('Fecha de Creación', auto_now_add=True)
    fec_m = models.DateTimeField('Fecha de Modificación', auto_now=True)
    fec_e = models.DateTimeField('Fecha de eliminación', auto_now=True)
    #user_c = models.ForeignKey(User,on_delete=models.CASCADE)
    user_m = models.IntegerField('Usuario que modifica', blank=True, null=True)

    class Meta:
        abstract = True