from django.urls import path
from django.contrib.auth import views as auth_views
from .views import Home, HomeSinPrivilegio

urlpatterns = [
    path('',Home.as_view(), name='home'),
    path('login/',auth_views.LoginView.as_view(template_name='accounts/login.html'),name='login'),
    path('logout/',auth_views.LogoutView.as_view(template_name='accounts/login.html'),name='logout'),
    path('sin_privilegios/',HomeSinPrivilegio.as_view(), name='sin_privilegios'),
]