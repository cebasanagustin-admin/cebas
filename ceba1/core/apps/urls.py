from django.urls import path
from .views import *
urlpatterns = [

#====Curso====#
    path('cursos/list',CursoListar.as_view(), name='curso_list'),
    path('cursos/new',CursoCreate.as_view(), name='curso_new'),
    path('cursos/edit/<int:pk>', CursoEdit.as_view(), name='curso_edit'),

#====Estudiante====#
    path('estudiantes/list',EstudianteListar.as_view(), name='estudiante_list'),
    path('estudiantes/new',EstudianteCreate.as_view(), name='estudiante_new'),
    path('estudiantes/edit/<int:pk>', EstudianteEdit.as_view(), name='estudiante_edit'),
    path('estudiantes/detalle/<int:pk>/', EstudianteDetalle.as_view(), name='estudiante_detalle'),

#====Pagos====#
    path('pagos/list',PagosListar.as_view(), name='pagos_list'),
    path('pagos/new',PagosCreate.as_view(), name='pagos_new'),
    path('pagos/detalle/<int:pk>/', PagosDetalle.as_view(), name='pagos_detalle'),
    path('pagos/edit/<int:pk>', PagosEdit.as_view(), name='pagos_edit'),
    path('pagos/inactivar/<int:id>', pagos_inactivar, name='pagos_inactivar'),
    #==Reporte/exportar PDF==#
    path('pagos/export_pdf/', export_pdf_pagos, name='pagos_export'),
    path('pagos/exportar/<int:pk>', Export_Pagos.as_view(), name='pagos_exportar'),


 #====Calificacion====#
    path('calificaciones/list',CalificacionListar.as_view(), name='calificacion_list'),
    path('calificaciones/new',CalificacionCreate.as_view(), name='calificacion_new'),
    path('calificaciones/edit/<int:pk>', CalificacionEdit.as_view(), name='calificacion_edit'),
    path('calificaciones/detalle/', CalificacionDetalle.as_view(), name='calificacion_detalle'),

#Docente#
    path('docentes/list',DocenteListar.as_view(), name='docente_list'),
    path('docentes/new',DocenteCreate.as_view(), name='docente_new'),
    path('docentes/edit/<int:pk>', DocenteEdit.as_view(), name='docente_edit'),
    path('docentes/detalle/<int:pk>/', DocenteDetalle.as_view(), name='docente_detalle'),


]