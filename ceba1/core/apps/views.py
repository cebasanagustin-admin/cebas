from urllib import response
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import PermissionRequiredMixin
from braces.views import LoginRequiredMixin, GroupRequiredMixin
from django.views import generic, View
from django.urls import reverse_lazy
from .models import *
from .forms import *
from django.contrib import messages

# Create your views here.

from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.template.loader import get_template
from weasyprint import HTML
import tempfile
from django.db.models import Sum

import os

############################--Curso--############################
class StaffRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            return redirect(reverse_lazy('admin:login'))
        return super (StaffRequiredMixin, self).dispatch(request, *args, **kwargs)
    


class CursoListar(StaffRequiredMixin, LoginRequiredMixin, generic.ListView):
    permission_required = "listar_curso"
    model = Mdl2HCourse
    template_name = 'curso/curso_list.html'
    context_object_name = 'cursoobj'
    login_url = 'base:login'

class CursoCreate(LoginRequiredMixin, GroupRequiredMixin, generic.CreateView):
    group_required = ['Docente']
    model = Mdl2HCourse
    template_name = 'curso/curso.html'
    context_object_name = 'cursoobj'
    form_class =CursoForm
    success_url = reverse_lazy('academico:curso_list')
    login_url = 'base:login'

    def form_valid(self, form):
        form.instance.user_c = self.request.user
        print(self.request.user.id)
        return super().form_valid(form)

class CursoEdit(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    permission_required="edit_curso"
    model = Mdl2HCourse
    template_name = "curso/curso.html"
    context_object_name = "cursoobj"
    form_class = CursoForm
    success_url = reverse_lazy("academico:curso_list")
    login_url = 'base:login'


############################--Estudiante--############################

class EstudianteListar(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    permission_required = "cmp.view_proveedor"
    model = Mdl2HUser
    template_name = 'estudiante/estudiante_list.html'
    context_object_name = 'estudianteobj'
    login_url = 'base:login'

class EstudianteCreate(LoginRequiredMixin, generic.CreateView):
    model = Mdl2HUser
    template_name = 'estudiante/estudiante.html'
    context_object_name = 'estudianteobj'
    form_class = EstudianteForm
    success_url = reverse_lazy('academico:estudiante_list')
    login_url = 'base:login'

    def form_valid(self, form):
        form.instance.user_c = self.request.user
        print(self.request.user.id)
        return super().form_valid(form)

class EstudianteEdit(LoginRequiredMixin, generic.UpdateView):
    model = Mdl2HUser
    template_name = "estudiante/estudiante.html"
    context_object_name = 'estudianteobj'
    form_class = EstudianteForm
    success_url = reverse_lazy('academico:estudiante_list')
    login_url = 'base:login'

    def form_valid(self, form):
        form.instance.user_m = self.request.user.id
        return super().form_valid(form)

class EstudianteDetalle(LoginRequiredMixin, generic.DetailView):
    model = Mdl2HUser
    template_name = 'estudiante/detalle.html'
    context_object_name = 'estudianteobj'


############################--Pagos--############################

def CrearPagos(request):
    if request.method == 'POST':
        pagos_form = PagosForm(request.POST)
        if pagos_form.is_valid():                        
            pagos_form.save()
            messages.info(request, "Registro añadido con éxito")            
        
    else:
            pagos_form = PagosForm()
    estu = Mdl2HUser.objects.all()
    
    return render(request, 'pagos/pagos_crear.html', {'pagos_form':pagos_form, 'estu':estu})


class PagosListar(StaffRequiredMixin,  generic.ListView):
    permission_required = "listar_pagos"
    model = Pagos
    template_name = 'pagos/pagos_list.html'
    context_object_name = 'pagosobj'
    prod=Pagos.objects.filter(estado=True)
    login_url = 'base:login'    



class PagosCreate(StaffRequiredMixin, LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    permission_required = "crear_pagos"
    model = Pagos
    template_name = 'pagos/pagos.html'
    context_object_name = 'pagosobj'
    form_class = PagosForm
    success_url = reverse_lazy('academico:pagos_list')
    login_url = 'base:login'
    success_message="Pago Registrado Satisfactoriamente"
    

class PagosDetalle(LoginRequiredMixin, generic.DetailView):
    model = Pagos
    template_name = 'pagos/detalle.html'
    context_object_name = 'pagosobj'   


class PagosEdit(LoginRequiredMixin, generic.UpdateView):
    model = Pagos
    template_name = "pagos/pagos.html"
    context_object_name = "pagosobj"
    form_class = PagosForm
    success_url = reverse_lazy("academico:pagos_list")
    login_url = 'base:login'

    def form_valid(self, form):
        form.instance.user_m = self.request.user.id
        return super().form_valid(form)

def pagos_inactivar(request, id):
    pgs = Pagos.objects.filter(pk=id).first()
    contexto={}
    template_name="pagos/pagos_del.html"

    if not pgs:
        redirect("academico:pagos_list")

    if request.method=='GET':
        contexto={'obj':pgs}

    if request.method=='POST':
        pgs.estado=False
        pgs.save()
        messages.success(request, 'Marca Inactivada')
        return redirect("academico:pagos_list")

    return render(request,template_name,contexto)

############################--Calificación--############################

class CalificacionListar(LoginRequiredMixin, generic.ListView):
    permission_required = "cmp.view_proveedor"
    model = Calificacion
    template_name = 'calificacion/calificacion_list.html'
    context_object_name = 'calificacionobj'
    login_url = 'base:login'


class CalificacionCreate(LoginRequiredMixin, generic.CreateView):
    model = Calificacion
    template_name = 'calificacion/calificacion.html'
    context_object_name = 'calificacionobj'
    form_class = CalificacionForm
    success_url = reverse_lazy('academico:calificacion_list')
    login_url = 'base:login'


class CalificacionEdit(LoginRequiredMixin, generic.UpdateView):
    model = Calificacion
    template_name = "calificacion/calificacion.html"
    context_object_name = "calificacionobj"
    form_class = PagosForm
    success_url = reverse_lazy("academico:calificacion_list")
    login_url = 'base:login'

    def form_valid(self, form):
        form.instance.user_m = self.request.user.id
        return super().form_valid(form)


class CalificacionDetalle(LoginRequiredMixin, generic.ListView):
    model = Calificacion
    template_name = 'calificacion/detalle.html'

    def get(self, request, *args, **kwargs):
        
        valor = request.GET.get('valor', '')
        lista_calificacion_estudiante = Calificacion.objects.filter(bimestre=valor)
        
        return render (request, 'calificacion/detalle.html', {'lista_calificacion_estudiante': lista_calificacion_estudiante})

    #def get_context_data(self, **kwargs):
        #context = super(CalificacionDetalle, self).get_context_data(**kwargs)
        #context['events'] =Calificacion.objects.filter(idestudiante_id=self.request.user)
        #context['calificacionobj'] = context['events']
        #context['paginate_by']=context['events']
        #return context

#class ResumenDetail(LoginRequiredMixin, generic.DetailView):
    #model = Estudiante
    #context_object_name = 'obj'
    #template_name = 'calificacion/resumen.html'

############################--Docente--############################

class DocenteListar(LoginRequiredMixin, generic.ListView):
    permission_required = "cmp.view_proveedor"
    model = Docente
    template_name = 'curso/docente_list.html'
    context_object_name = 'docenteobj'
    login_url = 'base:login'

class DocenteCreate(LoginRequiredMixin, generic.CreateView):
    model = Docente
    template_name = 'curso/docente.html'
    context_object_name = 'docenteobj'
    form_class =DocenteForm
    success_url = reverse_lazy('academico:docente_list')
    login_url = 'base:login'

    def form_valid(self, form):
        form.instance.user_c = self.request.user
        print(self.request.user.id)
        return super().form_valid(form)


class DocenteEdit(LoginRequiredMixin, generic.UpdateView):
    model = Docente
    template_name = "curso/docente.html"
    context_object_name = "docenteobj"
    form_class = DocenteForm
    success_url = reverse_lazy("academico:docente_list")
    login_url = 'base:login'

    def form_valid(self, form):
        form.instance.user_m = self.request.user.id
        return super().form_valid(form)

class DocenteDetalle(LoginRequiredMixin, generic.DetailView):
    model = Docente
    template_name = 'curso/detalle_docente.html'
    context_object_name = 'docenteobj'



#=============================================================================================#      
#===================================-->Reportes PDF - WeasyPrint<--============================# 
def export_pdf_pagos(request):
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition']='inline; attachment; filename=Expenses' +'.pdf'
    response['Content-Transfer-Encoding'] = 'binary'

    pagos = Pagos.objects.all()

    html_string = render_to_string(
        'pagos/export_pdf_pagos.html',{'pagos': pagos})
    html = HTML(string=html_string)

    result=html.write_pdf()

    with tempfile.NamedTemporaryFile(delete=True) as output:
        output.write(result)
        output.flush()
        output=open(output.name, 'rb')
        response.write(output.read())
    
    return response


class Export_Pagos(View):
    def get(self, request, *args, **kwargs):
        try:
            template = get_template('pagos/export_pdf_pagos.html')
            context = {
                'pago':Pagos.objects.get(pk=self.kwargs['pk']),
                'comp':{'name': 'CEBA-San Agustin'},
            }
            html=template.render(context)
            pdf=HTML(string=html).write_pdf()
            return HttpResponse(pdf, content_type='application/pdf')            
        except:
            pass
        return HttpResponseRedirect(reverse_lazy('academico:pagos_list'))