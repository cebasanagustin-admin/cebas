# Generated by Django 2.2.24 on 2022-02-09 05:30

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0006_docente'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='docente',
            table='mdl2h_docente',
        ),
    ]
